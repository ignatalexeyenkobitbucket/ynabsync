package ignat.ynabsync.diff;

import ignat.ynabsync.parser.ParsedEntry;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Simple diff based on diff counting operations by dates and amounts
 *
 * @since v1.0
 */
public class YnabDiff {

    /**
     * Filters the elements that only withing the time range [start_date, end date]
     *
     * @param collection collection to filter
     * @param startDate  start date
     * @param endDate    end date
     * @return new collection that contains filtered items
     */
    public Collection<ParsedEntry> filterByDate(List<ParsedEntry> collection, final Date startDate, final Date endDate) {

        // entry.date >= start_date and entry.date <= end_date
        final List<ParsedEntry> resultList = collection.stream().filter(
                entry -> (entry.getDate().after(startDate) || startDate.equals(entry.getDate()))
                        && (entry.getDate().before(endDate) || endDate.equals(entry.getDate()))
        ).collect(Collectors.<ParsedEntry>toList());


        return resultList;
    }

    /**
     * Counts occurence of given price in the table.
     * price -> count
     *
     * @param sourceTransactions
     * @return
     */
    public Map<Integer, Integer> countPriceEntries(List<ParsedEntry> sourceTransactions) {
        // price -> number of entries
        Map<Integer, Integer> priceCounter = new HashMap<>();

        for (ParsedEntry sourceTransaction : sourceTransactions) {
            Integer key = sourceTransaction.getAmount();
            if (priceCounter.containsKey(key)) {
                final Integer count = priceCounter.get(key);
                priceCounter.put(key, count + 1);
            } else {
                priceCounter.put(key, 1);
            }
        }
        return priceCounter;
    }

    /**
     * Calculates how much price repeats in source and target tables
     *
     * @param source a source of transactions
     * @param target target for this context, just another source of transactions
     * @return Map Amount -> (Source Count, Target Count)
     */
    public Map<Integer, DiffValue> fillInCounterTable(List<ParsedEntry> source, List<ParsedEntry> target) {
        Map<Integer, Integer> sourceDiffCount = countPriceEntries(source);
        Map<Integer, Integer> targetDiffCount = countPriceEntries(target);

        Map<Integer, DiffValue> countTable = new HashMap<Integer, DiffValue>();

        // first - init collections with empty counters
        for (Integer amount : sourceDiffCount.keySet()) {
            countTable.put(amount, new DiffValue());
        }
        for (Integer amount : targetDiffCount.keySet()) {
            countTable.put(amount, new DiffValue());
        }

        // second run - filling the counting table
        for (Integer amount : sourceDiffCount.keySet()) {
            DiffValue diffValue = countTable.get(amount);
            diffValue.setSourceAmount(sourceDiffCount.get(amount));
        }
        for (Integer amount : targetDiffCount.keySet()) {
            DiffValue diffValue = countTable.get(amount);
            diffValue.setTargetAmount(targetDiffCount.get(amount));
        }

        return countTable;
    }

    /**
     * Calculates which transactions do not match by counting how often do they present in a table
     *
     * @param source
     * @param target
     * @return
     */
    public Collection<ParsedEntry> findMissingTransactionsByCount(List<ParsedEntry> source, List<ParsedEntry> target) {
        List<ParsedEntry> result = new ArrayList<>();

        final Map<Integer, DiffValue> countTable = fillInCounterTable(source, target);

        for (ParsedEntry transaction : source) {
            Integer key = transaction.getAmount();
            DiffValue diffValue = countTable.get(key);
            Integer targetCount = diffValue.getTargetAmount();
            Integer sourceCount = diffValue.getSourceAmount();

            // source is missing transaction ( or target has a redundant one )
            if (sourceCount > targetCount) {
                result.add(transaction);
            } else {
                // if sourceCount == targetCount: NOOP, we are all good, exact number of transactions
                // if sourceCount < targetCount: NOOP, well we would need another ran in reversed arguments
            }
        }

        return result;
    }

    static class DiffValue {
        int sourceAmount;
        int targetAmount;

        public int getSourceAmount() {
            return sourceAmount;
        }

        public void setSourceAmount(int sourceAmount) {
            this.sourceAmount = sourceAmount;
        }

        public int getTargetAmount() {
            return targetAmount;
        }

        public void setTargetAmount(int targetAmount) {
            this.targetAmount = targetAmount;
        }
    }
}
