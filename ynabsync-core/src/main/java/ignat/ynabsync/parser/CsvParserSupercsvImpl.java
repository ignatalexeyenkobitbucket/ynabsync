package ignat.ynabsync.parser;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.supercsv.io.CsvListReader;
import org.supercsv.io.ICsvListReader;
import org.supercsv.prefs.CsvPreference;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * @since v1.0
 */
public class CsvParserSupercsvImpl {

    static final Logger logger = LogManager.getLogger(CsvParserSupercsvImpl.class.getName());

    public List<ParsedEntry> parseWithDelimiters(Reader fileReader, Character quoteChar, Character delimiterChar, String endOfLineSymbols) {
        final ICsvListReader reader = new CsvListReader(fileReader,
                new CsvPreference.Builder(quoteChar, delimiterChar, endOfLineSymbols).surroundingSpacesNeedQuotes(true).build());

        int lineNr = 0;

        // initial run - count distribution
        Collection<String> line = readLine(reader);
        List<Collection<String>> listOfCsvColumnValues = new ArrayList<>();
        listOfCsvColumnValues.add(line);

        int maxColumnNumber = 0;
        while (line != null) {
            logger.debug("Line nr: " + lineNr);

            if (line.size() > maxColumnNumber) {
                maxColumnNumber = line.size();
            }

            line = readLine(reader);
            if (line != null) {
                listOfCsvColumnValues.add(line);
                lineNr++;
            }
        }

        removeMalformedRows(listOfCsvColumnValues, maxColumnNumber);
        final Collection<String> potentialHeader = new ArrayList<>(listOfCsvColumnValues.get(0));
        // check if contains header column
        removeRowsWithoutNumbers(listOfCsvColumnValues);
        // remove rows without a date column
        removeRowsWithoutDateColumn(listOfCsvColumnValues);


        List<ParsedEntry> parsedEntryList = new ArrayList<>(listOfCsvColumnValues.size());
        for (Collection<String> csvColumnValue : listOfCsvColumnValues) {
            parsedEntryList.add(parseEntryFromCsvColumnValues(csvColumnValue));
        }

        return parsedEntryList;
    }

    /**
     * Composes a parsed entry out of a CSV row
     *
     * @param csvColumnValue list of strings represent csv column value each.
     * @return a parsed entity
     */
    ParsedEntry parseEntryFromCsvColumnValues(Collection<String> csvColumnValue) {
        // Data arrays to aggregate parsed data.
        // Decision in mapping the data is done at the very end of the run.
        List<Date> parsedDateList = new ArrayList<>(csvColumnValue.size());
        List<Integer> parsedCentList = new ArrayList<>(csvColumnValue.size());
        List<String> parsedMemoList = new ArrayList<>(csvColumnValue.size());

        for (String column : csvColumnValue) {
            final String trimmedColumn = StringUtils.trim(column);
            if (StringUtils.isEmpty(trimmedColumn)) {
                continue;
            }

            Date date = null;
            try {
                date = tryParseSupportedDates(trimmedColumn);
                parsedDateList.add(date);
                continue;
            } catch (RuntimeException e) {
                // columns must be validated earlier
                // this exception only means we attempted to parse wrong column, so we'll ignore it
                logger.debug("Column is not a date column. Column = '" + trimmedColumn + "'");
            }

            Integer cents;
            try {
                cents = new ParsedEntry().parseCents(extractCurrencyName(trimmedColumn));
                parsedCentList.add(cents);
                continue;
            } catch (Exception e) {
                // columns must be validated earlier
                // this exception only means we attempted to parse wrong column, so we'll ignore it
                logger.debug("Column is not a money column. Column = '" + trimmedColumn + "'");
            }

            // landing here means we did not parse no date no money value, accepting this as a string
            parsedMemoList.add(trimmedColumn);
        }
        final ParsedEntry parsedEntry = buildParsedEntry(parsedDateList, parsedCentList, parsedMemoList);
        return parsedEntry;

    }

    /**
     * Creates parsed entry and populates it with the data.
     *
     * @param parsedDateList list of the parsed dates from a csv row
     * @param parsedCentList list of parsed amount (in cents) from a csv row
     * @param parsedMemoList list of parsed memos (strings) from a csv row
     */
    private ParsedEntry buildParsedEntry(List<Date> parsedDateList, List<Integer> parsedCentList, List<String> parsedMemoList) {
        ParsedEntry parsedEntry = new ParsedEntry();

        // Use actual operation date as a first recorded date
        parsedEntry.setDate(parsedDateList.get(0));
        if (parsedDateList.size() > 1) {
            // if there are additional date, we consider this to be the accounting date
            parsedEntry.setAccountDate(parsedDateList.get(1));
        }

        // If there is only one number - it'll be parsed as the operation amount
        parsedEntry.setAmountInCents(parsedCentList.get(0));
        if (parsedCentList.size() > 1) {
            // additional numbers would be considered as total balance change
            parsedEntry.setBalanceInCents(parsedCentList.get(1));
        }

        // all operations that did not parse as date or number are added as memos
        parsedEntry.setMemo(parsedMemoList);

        return parsedEntry;
    }

    /**
     * Attempts to parse a given string as a date
     * CSV files might use different date formats. We don't know which column is which.
     *
     * @param column csv column that needs to be parsed.
     * @return date that is parsed from the given format
     */
    private Date tryParseSupportedDates(String column) {
        Date parsedDate = null;
        String[] supportedFormats = {"YYYY-MM-DD", "DDD MM YYYY", "DD.MM.YYYY", "dd/MM/yyyy"};

        for (String supportedFormat : supportedFormats) {
            try {
                parsedDate = new SimpleDateFormat(supportedFormat).parse(column);
            } catch (ParseException e) {
                logger.debug("Can not parse date");
            }
            if (parsedDate != null) {
                return parsedDate;
            }
        }

        throw new IllegalArgumentException("Can not parse as date. column = " + column);
    }

    /**
     * Removes rows in place which are missing dates.
     * <p>
     * Rationale: transactional date is something that is required for the import.
     * Rows without the date are considered invalid.
     *
     * @param listOfCsvColumnValues list to validate for missing data.
     */
    private void removeRowsWithoutDateColumn(List<Collection<String>> listOfCsvColumnValues) {

        final Iterator<Collection<String>> csvListIterator = listOfCsvColumnValues.iterator();
        while (csvListIterator.hasNext()) {
            int failedParse = 0;
            final Collection<String> theRow = csvListIterator.next();
            for (String column : theRow) {
                try {
                    Date parsedDate = tryParseSupportedDates(column);
                } catch (RuntimeException e) {
                    failedParse++;
                }
            }

            if (failedParse == theRow.size()) {
                // we did not parse a single row - the column is a header
                logger.info("Detected a malformed row without a date. It will be skipped from parsing. Header = " + theRow);
                csvListIterator.remove();
            }
        }

    }

    /**
     * Removes rows in place which are missing numbers.
     * <p>
     * Rationale: transactional amount is something that is required for the import.
     * Rows without the amount are considered invalid.
     *
     * @param listOfCsvColumnValues list to validate for missing data.
     */
    private void removeRowsWithoutNumbers(List<Collection<String>> listOfCsvColumnValues) {
        final Iterator<Collection<String>> csvListIterator = listOfCsvColumnValues.iterator();
        while (csvListIterator.hasNext()) {

            int failedParse = 0;
            final Collection<String> theRow = csvListIterator.next();
            ParsedEntry parsedEntry = new ParsedEntry();

            for (String column : theRow) {

                final String columnNoCurrencyName = extractCurrencyName(column);
                try {
                    parsedEntry.parseCents(columnNoCurrencyName);
                } catch (RuntimeException e) {
                    failedParse++;
                }
            }

            if (failedParse == theRow.size()) {
                // we did not parse a single row - the column is a header
                logger.info("Detected a malformed row (could be a header row). It will be skipped from parsing. Header = " + theRow);
                csvListIterator.remove();
            }
        }
    }

    /**
     * Replaces the known currency name with nothing
     * Sometimes currency name is inlcuded in the import which breaks parsing numberical value
     * This method removes known used currency names
     * @param column csv column that could contain the symbol
     * @return value without the known currency name
     */
    private String extractCurrencyName(String column) {
        String[] supportedCurrencyNames = {"PLN", "USD", "EUR", "JPY", "CAD", "GBP", "CZK", "DKK", "HUF", "ISK", "CHF", "MDL", "NOK", "RUB", "RSD", "TRY", "UAH"};
        for (String supportedCurrencyName : supportedCurrencyNames) {
            if (StringUtils.contains(column, supportedCurrencyName)) {
                return StringUtils.remove(column, supportedCurrencyName);
            }
        }
        return column;
    }

    /**
     * Removes (in place) garbage rows from the csv export file
     * <p>
     * Context: CSV files could contain some back header and footer data along the actual data.
     * After initial run we know how many CSV columns we should expect for the data export.
     * All the columns that do not match are being removed.
     *
     * @param listOfCsvColumnValues list of all read lines that contain garbage rows
     * @param expectedColumnNumbers expected number of rows in an export line.
     */
    private void removeMalformedRows(List<Collection<String>> listOfCsvColumnValues, int expectedColumnNumbers) {
        final Iterator<Collection<String>> csvListIterator = listOfCsvColumnValues.iterator();
        while (csvListIterator.hasNext()) {
            final Collection<String> theRow = csvListIterator.next();
            if (theRow.size() < expectedColumnNumbers) {
                logger.info("Removing the row as it has only " + theRow.size() + " column. But max in file is " + expectedColumnNumbers);
                csvListIterator.remove();
            }
        }
    }

    /**
     * Utility method that wraps read line. Syntax sugar.
     *
     * @param reader reader to be used
     * @return list of strings that represent csv column value each.
     */
    private Collection<String> readLine(ICsvListReader reader) {
        try {
            return reader.read();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public List<ParsedEntry> parseFromStreamWithDelimiters(InputStream resourceAsStream, Character quoteChar, Character delimiterChar, String endOfLineSymbols) {
        return this.parseWithDelimiters(new InputStreamReader(resourceAsStream), quoteChar, delimiterChar, endOfLineSymbols);
//        return this.parseWithDelimiters(new InputStreamReader(resourceAsStream, "windows-1250"), quoteChar, delimiterChar, endOfLineSymbols);
    }
}
