package ignat.ynabsync.parser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @since v1.0
 */
public class ParsedEntry {
    private Integer operationId;
    private Date date;
    private String dateRaw;
    private Date accountDate;
    private String accountDateRaw;
    private String title;
    private List<String> memo = new ArrayList<>();
    private String payee;
    private String payeeRaw;
    private String category;
    private String amountRaw;
    private Integer amount;
    private Integer balance;

    public void setPayee(String payee) {
        this.payee = payee;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategory() {
        return this.category;
    }

    public String getPayee() {
        return payee;
    }

    public List<String> getMemo() {
        return new ArrayList<String>(memo);
    }

    public String getPayeeRaw() {
        return payeeRaw;
    }

    public String getTitle() {
        return title;
    }

    public void setMemo(List<String> memo) {
        this.memo = new ArrayList<String>(memo);
    }

    public void addMemo(String memo) {
        this.memo.add(memo);
    }

    public Date getDate() {
        return date;
    }

    /**
     * Sets the dateRaw and Date.
     *
     * @param dateRaw date as string in dd/MM/yyyy format
     * @throws RuntimeException if format does not match
     */
    public void setDateFromString(String dateRaw) {
        this.dateRaw = dateRaw;
        try {
            this.date = new SimpleDateFormat("dd/MM/yyyy").parse(dateRaw);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    public void setAmountInCents(Integer cents) {
        this.amountRaw = String.valueOf((cents * 1.0F) / 100);
        this.amount = cents;
    }

    public void setAmountFloat(double v) {
        this.amountRaw = String.valueOf(v);
        final long amountValueRounded = Math.round(v * 100);
        if (amountValueRounded >= Integer.MAX_VALUE) {
            throw new IllegalStateException("Value of the amount is too big and can not be converted. amountValueRounded = " + amountValueRounded);
        }
        this.amount = (int) amountValueRounded;
    }

    public void setAmountFloat(String amountRaw) {
        this.amountRaw = amountRaw;
        this.amount = this.parseCents(amountRaw);
    }

    Integer parseCents(final String amountRaw) {
        String clearnedInputAmount = amountRaw.replaceAll(" ", "");
        int sign = 1;
        if ('-' == clearnedInputAmount.charAt(0)) {
            clearnedInputAmount = clearnedInputAmount.substring(1);
            sign = -1;
        }

        // NOTE: parsing Polish separator
        if (clearnedInputAmount.contains(",")) {
            final String[] splittedParts = clearnedInputAmount.split(",", 0);
            String fullMoneyUnit = splittedParts[0];
            String cents = splittedParts[1];
            int parsedCents = sign * (100 * Integer.parseInt(fullMoneyUnit) + Integer.parseInt(cents));
            return parsedCents;
        }

        throw new IllegalStateException("Not supported number format. clearnedInputAmount = '" + clearnedInputAmount + "'");
    }

    public int getAmount() {
        return amount;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setAccountDate(Date accountDate) {
        this.accountDate = accountDate;
    }

    public Date getAccountDate() {
        return accountDate;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalanceInCents(Integer balance) {
        this.balance = balance;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * We consider both parsed entries to be equal if they have the same amount and the date
     *
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParsedEntry that = (ParsedEntry) o;
        return getDate().equals(that.getDate()) &&
                amount.equals(that.amount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDate(), amount);
    }

    @Override
    public String toString() {
        return "ParsedEntry{" +
                "operationId=" + operationId +
                ", date=" + date +
                ", dateRaw='" + dateRaw + '\'' +
                ", accountDate=" + accountDate +
                ", accountDateRaw='" + accountDateRaw + '\'' +
                ", title='" + title + '\'' +
                ", memo='" + memo + '\'' +
                ", payee='" + payee + '\'' +
                ", payeeRaw='" + payeeRaw + '\'' +
                ", category='" + category + '\'' +
                ", amountRaw=" + amountRaw +
                ", amount=" + amount +
                '}';
    }
}
