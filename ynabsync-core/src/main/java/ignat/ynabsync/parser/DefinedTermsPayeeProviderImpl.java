package ignat.ynabsync.parser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Parses the payee based on the defined terms
 *
 * @since v1.0
 */
public class DefinedTermsPayeeProviderImpl implements PayeeProvider {

    private final DefinedDictionaryParser parser;

    public DefinedTermsPayeeProviderImpl(final Map<String, List<String>> dictionaryTerms) {
        this.parser = new DefinedDictionaryParser(dictionaryTerms);
    }

    public void parsePayeeFromTerms(ParsedEntry parsedEntry, Collection<String> hintList) {
        List<String> allTerms = new ArrayList<String>();

        addIfNotEmptyOrNull(allTerms, parsedEntry.getMemo());
        addIfNotEmptyOrNull(allTerms, parsedEntry.getTitle());
        addIfNotEmptyOrNull(allTerms, parsedEntry.getPayeeRaw());

        if (hintList != null) {
            allTerms.addAll(hintList);
        }

        final String payee = parser.parseEntry(allTerms);
        if (parsedEntry.getPayee() != null) {
            parsedEntry.setPayee(payee);
        }
    }

    private void addIfNotEmptyOrNull(List<String> allTerms, String entry) {
        if (entry != null && !entry.equals("")) {
            allTerms.add(entry);
        }
    }

    private void addIfNotEmptyOrNull(List<String> allTerms, List<String> entry) {
        if (entry != null) {
            allTerms.addAll(entry);
        }
    }
}
