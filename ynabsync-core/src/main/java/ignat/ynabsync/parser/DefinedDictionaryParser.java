package ignat.ynabsync.parser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Dictionary.
 * Used for tagging known payees and categories
 *
 * @since v1.0
 */
public class DefinedDictionaryParser {

    // safe map stores [parsed tag] -> list[known mapping] (1:n)
    private final Map<String, List<String>> safeMap;
    // safe index stores [known mapping] -> [parsed tag] (1:1)
    // safe index meant to be used mainly for the lookups
    private final Map<String, String> safeIndex;

    public DefinedDictionaryParser(final Map<String, List<String>> dictionaryTerms) {
        // deep copy of the map that passed

        this.safeMap = new ConcurrentHashMap<String, List<String>>(dictionaryTerms);
        for (String term : dictionaryTerms.keySet()) {
            final List<String> valuesList = dictionaryTerms.get(term);
            this.safeMap.put(term, new ArrayList<String>(valuesList));
        }

        // initialises safe index
        safeIndex = new ConcurrentHashMap<String, String>();
        for (String term : this.safeMap.keySet()) {
            final List<String> valuesList = dictionaryTerms.get(term);
            for (String value : valuesList) {
                if (value == null) {
                    continue;
                }

                if (safeIndex.containsKey(value)) {
                    throw new IllegalStateException("Index already contains key for value = '" + value + "'");
                } else {
                    safeIndex.put(value, term);
                }
            }
        }
    }

    public String parseEntry(Collection<String> terms) {
        for (String term : terms) {
            if (term != null) {
                String termLower = term.toLowerCase();

                // checking all dictionary terms in a term
                for (String indexKey : this.safeIndex.keySet()) {
                    if (termLower.contains(indexKey.toLowerCase())) {
                        String dictionaryWord = this.safeIndex.get(indexKey);
                        return dictionaryWord;
                    }

                }
            }
        }
        return null;
    }
}
