package ignat.ynabsync.parser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Parses the category based on the defined terms
 *
 * @since v1.0
 */
public class DefinedTermsCategoryProviderImpl implements CategoryProvider {

    private final DefinedDictionaryParser parser;

    public DefinedTermsCategoryProviderImpl(final Map<String, List<String>> dictionaryTerms) {
        parser = new DefinedDictionaryParser(dictionaryTerms);
    }

    public void parseCategoryFromTerms(ParsedEntry parsedEntry, Collection<String> hintList) {
        List<String> allTerms = new ArrayList<String>();

        addIfNotEmptyOrNull(allTerms, parsedEntry.getPayee());
        addIfNotEmptyOrNull(allTerms, parsedEntry.getMemo());
        addIfNotEmptyOrNull(allTerms, parsedEntry.getPayeeRaw());

        if (hintList != null) {
            allTerms.addAll(hintList);
        }

        final String category = parser.parseEntry(allTerms);
        if (category != null) {
            parsedEntry.setCategory(category);
        }

    }

    private void addIfNotEmptyOrNull(List<String> allTerms, String entry) {
        if (entry != null && !entry.equals("")) {
            allTerms.add(entry);
        }
    }

    private void addIfNotEmptyOrNull(List<String> allTerms, List<String> entry) {
        if (entry != null) {
            allTerms.addAll(entry);
        }
    }
}
