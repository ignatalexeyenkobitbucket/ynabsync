package ignat.ynabsync.parser;

import java.util.Collection;

/**
 * @since v1.0
 */
public interface PayeeProvider {
    void parsePayeeFromTerms(ParsedEntry parsedEntry, Collection<String> terms);
}
