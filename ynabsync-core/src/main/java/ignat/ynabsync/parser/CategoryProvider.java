package ignat.ynabsync.parser;

import java.util.Collection;

/**
 * @since v1.0
 */
public interface CategoryProvider {
    void parseCategoryFromTerms(ParsedEntry parsedEntry, Collection<String> terms);
}
