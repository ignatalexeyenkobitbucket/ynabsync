package ignat.ynabsync.parser;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

class CsvParserSupercsvImplTest {

    private CsvParserSupercsvImpl csvParserSupercsv = new CsvParserSupercsvImpl();

    @Test
    public void testCsvParser() {

        try (final InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream("csv/test_eKonto_1.csv");) {
            this.csvParserSupercsv = new CsvParserSupercsvImpl();
            final List<ParsedEntry> parsedEntries = csvParserSupercsv.parseFromStreamWithDelimiters(new BufferedInputStream(resourceAsStream), '"', ';', "\n");

            Assertions.assertEquals(8, parsedEntries.size());

            final ParsedEntry parsedEntry = parsedEntries.get(7);
            Assertions.assertEquals("ZAKUP PRZY U�YCIU KARTY", parsedEntry.getMemo().get(0));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    @Test
    public void testParseYnabFormatFrom_11_2019_few_transactions(){

        try (final InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream("csv/test_eKonto_10_export-format-11-2019.csv");) {
            this.csvParserSupercsv = new CsvParserSupercsvImpl();
            final List<ParsedEntry> parsedEntries = csvParserSupercsv.parseFromStreamWithDelimiters(new BufferedInputStream(resourceAsStream), '"', ';', "\n");

            final ParsedEntry ynabEntry0 = parsedEntries.get(0);
            Assertions.assertEquals("Spotify PD88888888  ZAKUP PRZY U�YCIU KARTY - INTERNET", ynabEntry0.getMemo().get(0));
            Assertions.assertEquals(-2999, ynabEntry0.getAmount());

            final ParsedEntry ynabEntry1 = parsedEntries.get(1);
            Assertions.assertEquals("JANUSZ KOWALSKI, PRZELEW �RODK�W  UL.DLUGA 1 M.88    80-827 GDA�SK                       PRZELEW WEWN�TRZNY PRZYCHODZ�CY                                                   88888888888888888888888888", ynabEntry1.getMemo().get(0));
            Assertions.assertEquals(50000, ynabEntry1.getAmount());

            final ParsedEntry ynabEntry2 = parsedEntries.get(2);
            Assertions.assertEquals("PIEKARNIA CUKIERNIA MIELN  ZAKUP PRZY U�YCIU KARTY W KRAJU", ynabEntry2.getMemo().get(0));
            Assertions.assertEquals(-1357, ynabEntry2.getAmount());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void testParseMemoForDirectDebitsPolecenieZaplaty() {

        try (final InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream("csv/test_eKonto_11_polecenie_zaplaty.csv");) {
            this.csvParserSupercsv = new CsvParserSupercsvImpl();
            final List<ParsedEntry> parsedEntries = csvParserSupercsv.parseFromStreamWithDelimiters(new BufferedInputStream(resourceAsStream), '"', ';', "\n");

            final ParsedEntry parsedEntry = parsedEntries.get(0);
//            TODO: fix encoding to windows-1250
//            Assertions.assertEquals("OPŁATA MIES. ZA POLECENIE ZAPŁATY", parsedEntry.getMemo().get(0));
            Assertions.assertEquals("OP�ATA MIES. ZA POLECENIE ZAP�ATY", parsedEntry.getMemo().get(0));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    @Test
    public void testParseCsvEntry() {
        ParsedEntry parsedEntry = csvParserSupercsv.parseEntryFromCsvColumnValues(Arrays.asList("2015-05-25", "2015-05-25", "WYP�ATA W BANKOMACIE", "UL. GRUNWALDZKA 141/GDANSK                                            DATA TRANSAKCJI: 2015-05-25", "  ", "''", "-50,00", "2 322,27"));
        Assertions.assertEquals("WYP�ATA W BANKOMACIE", parsedEntry.getMemo().get(0));
        Assertions.assertEquals("UL. GRUNWALDZKA 141/GDANSK                                            DATA TRANSAKCJI: 2015-05-25", parsedEntry.getMemo().get(1));
        Assertions.assertEquals(toDate("2015-05-25"), parsedEntry.getDate());
        Assertions.assertEquals(toDate("2015-05-25"), parsedEntry.getAccountDate());
        Assertions.assertEquals(-5000, parsedEntry.getAmount());
        Assertions.assertEquals(232227, parsedEntry.getBalance());
    }

    private Date toDate(String dateAsStr) {
        try {
            return new SimpleDateFormat("YYYY-MM-DD").parse(dateAsStr);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

}