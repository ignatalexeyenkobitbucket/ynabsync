package ignat.ynabsync.parser;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Test basic parsing features
 */
class ParsedEntryTest {

    private CategoryProvider categoryProvider;
    private PayeeProvider payeeProvider;

    private void _initCategories() {
        final HashMap<String, List<String>> dictionaryTerms = new HashMap<String, List<String>>();
        dictionaryTerms.put("Everyday Expenses:Restaurants", Arrays.asList("Restauracja", "BURGER STACJA", "CUKIERNIA"));
        dictionaryTerms.put("Everyday Expenses:Software licenses", Collections.singletonList("Spotify"));
        dictionaryTerms.put("House Purchase: Bare flat purchase", Collections.singletonList("Ing Bank"));
        dictionaryTerms.put("Monthly Bills: Media and Admininstration", Collections.singletonList("Bamit"));
        dictionaryTerms.put("Everyday Expenses:Clothing", Collections.singletonList("Pepco"));
        dictionaryTerms.put("Everyday Expenses:Service fee", Collections.singletonList("PROWIZJA ALLEGRO"));
        categoryProvider = new DefinedTermsCategoryProviderImpl(dictionaryTerms);
    }

    private void _initPayees() {
        final HashMap<String, List<String>> dictionaryTerms = new HashMap<String, List<String>>();
        dictionaryTerms.put("Burger Stacja", Collections.singletonList("BURGER STACJA"));
        payeeProvider = new DefinedTermsPayeeProviderImpl(dictionaryTerms);
    }

    @BeforeEach
    public void init() {
        _initCategories();
        _initPayees();
    }

    @Test
    public void testParseCategoryOk() {
        ParsedEntry ynabEntry1 = new ParsedEntry();
        ynabEntry1.setCategory("");
        ynabEntry1.setPayee("");

        // Set different payee to set category
        payeeProvider.parsePayeeFromTerms(ynabEntry1, Collections.singletonList("GDANSK BURGER STACJA"));
        categoryProvider.parseCategoryFromTerms(ynabEntry1, Collections.singletonList(""));

        Assertions.assertEquals(ynabEntry1.getCategory(), "Everyday Expenses:Restaurants");
    }

    @Test
    public void testParseCategoryOk2() {
        ParsedEntry ynabEntry1 = new ParsedEntry();
        ynabEntry1.setCategory("");
        ynabEntry1.setPayee("");

        // Set different payee to set category
        ynabEntry1.addMemo("RESTAURACJA GRUBA R/Gdansk                                            DATA TRANSAKCJI: 2015-08-25");
        categoryProvider.parseCategoryFromTerms(ynabEntry1, Collections.singletonList(""));

        Assertions.assertEquals(ynabEntry1.getCategory(), "Everyday Expenses:Restaurants");
    }

    @Test
    public void testParseKnownPayees1() {
        ParsedEntry ynabEntry1 = new ParsedEntry();
        ynabEntry1.setPayee("Spotify P0D5CD621B  ZAKUP PRZY U�YCIU KARTY - INTERNET");
        ynabEntry1.addMemo("Spotify P0D5CD621B  ZAKUP PRZY U�YCIU KARTY - INTERNET");
        categoryProvider.parseCategoryFromTerms(ynabEntry1, Collections.singletonList(""));

        Assertions.assertEquals(ynabEntry1.getCategory(), "Everyday Expenses:Software licenses");
    }

    @Test
    public void testParseKnownPayees2() {
        ParsedEntry ynabEntry1 = new ParsedEntry();
        ynabEntry1.setPayee("");
        ynabEntry1.addMemo("PIEKARNIA CUKIERNIA MIELN  ZAKUP PRZY U�YCIU KARTY W KRAJU");
        categoryProvider.parseCategoryFromTerms(ynabEntry1, Collections.singletonList(""));

        Assertions.assertEquals(ynabEntry1.getCategory(), "Everyday Expenses:Restaurants");
    }

    @Test
    public void testParseKnownPayees3() {
        ParsedEntry ynabEntry1 = new ParsedEntry();
        ynabEntry1.setPayee("JANUSZ KOWALSKI ING BANK");
        ynabEntry1.addMemo("PRZELEW WYNAGRODZENIA");
        categoryProvider.parseCategoryFromTerms(ynabEntry1, Collections.singletonList(""));

        Assertions.assertEquals(ynabEntry1.getCategory(), "House Purchase: Bare flat purchase");
    }

    @Test
    public void testParseKnownPayees4() {
        ParsedEntry ynabEntry1 = new ParsedEntry();
        ynabEntry1.setPayee("BAMIT SP Z O. O.  UL. S. NOAKOWSKIEGO 3              80-313 GDANSK");
        ynabEntry1.addMemo("JANUSZ KOWALSKI, UL. XXXXKIEGO 88 / 88, 80-888 GDANSK. ROZLICZENIE MEDIďż˝W 01.07.2016 - 30.09.2016");
        categoryProvider.parseCategoryFromTerms(ynabEntry1, Collections.singletonList(""));

        Assertions.assertEquals(ynabEntry1.getCategory(), "Monthly Bills: Media and Admininstration");
    }

    @Test
    public void testParseKnownPayees5() {
        ParsedEntry ynabEntry1 = new ParsedEntry();
        ynabEntry1.setPayee("");
        ynabEntry1.addMemo("PEPCO 1819 GDANSK 1/GDANSK                                            DATA TRANSAKCJI: 2015-04-22");
        categoryProvider.parseCategoryFromTerms(ynabEntry1, Collections.singletonList(""));

        Assertions.assertEquals(ynabEntry1.getCategory(), "Everyday Expenses:Clothing");
    }

    @Test
    public void testParseKnownPayees6() {
        ParsedEntry ynabEntry1 = new ParsedEntry();
        ynabEntry1.setPayee("PAYU SPÓŁKA AKCYJNA  UL.GRUNWALDZKA 182                 60-166 POZNAďż˝ POLSKA");
        ynabEntry1.addMemo("PAYU XX9912565627XX PROWIZJA ALLEGRO (JANUSZKOWALSKI)");
        categoryProvider.parseCategoryFromTerms(ynabEntry1, Collections.singletonList(""));

        Assertions.assertEquals(ynabEntry1.getCategory(), "Everyday Expenses:Service fee");
    }

    @Test
    public void testParseKnownPayees7() {
        ParsedEntry ynabEntry1 = new ParsedEntry();
        ynabEntry1.setPayee("");
        ynabEntry1.addMemo("PEPCO 1819 GDANSK 1/GDANSK                                            DATA TRANSAKCJI: 2015-04-22");
        categoryProvider.parseCategoryFromTerms(ynabEntry1, Collections.singletonList(""));

        Assertions.assertEquals(ynabEntry1.getCategory(), "Everyday Expenses:Clothing");
    }

}