package ignat.ynabsync.diff;

import ignat.ynabsync.parser.ParsedEntry;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @since v1.0
 */
public class TestYnabDiff {

    YnabDiff ynabDiff = new YnabDiff();

    @Test
    void testFilterByDate() {

        ParsedEntry entry1 = new ParsedEntry();
        entry1.setDateFromString("28/02/2015"); // February

        ParsedEntry entry2 = new ParsedEntry();
        entry2.setDateFromString("14/03/2015"); // 14 March

        ParsedEntry entry3 = new ParsedEntry();
        entry3.setDateFromString("20/03/2015"); // 20 March

        ParsedEntry entry4 = new ParsedEntry();
        entry4.setDateFromString("1/05/2015"); // 1 May 2015

        List<ParsedEntry> collection = Arrays.asList(entry1, entry2, entry3, entry4);

        final String startDate = "01/03/2015";
        final String endDate = "31/03/2015";
        Collection<ParsedEntry> filtered = ynabDiff.filterByDate(collection, asDate(startDate), asDate(endDate));

        Assertions.assertEquals(2, filtered.size());
    }

    @Test
    void testEdgeFilterByDate() {

        ParsedEntry entry1 = new ParsedEntry();
        entry1.setDateFromString("01/03/2015"); // 1 March

        ParsedEntry entry2 = new ParsedEntry();
        entry2.setDateFromString("31/03/2015"); // 31 March

        List<ParsedEntry> collection = Arrays.asList(entry1, entry2);

        final String startDate = "01/03/2015";
        final String endDate = "31/03/2015";
        Collection<ParsedEntry> filtered = ynabDiff.filterByDate(collection, asDate(startDate), asDate(endDate));

        Assertions.assertEquals(2, filtered.size());
    }

    @Test
    public void testYnabRedundant() {
        ParsedEntry ynabEntry1 = new ParsedEntry();
        ynabEntry1.setAmountFloat(9.57);
        ynabEntry1.setDateFromString("12/03/2015");

        ParsedEntry ynabEntry2 = new ParsedEntry();
        ynabEntry2.setAmountFloat(-40.0);
        ynabEntry2.setDateFromString("30/03/2015");

        ParsedEntry mBankEntry1 = new ParsedEntry();
        mBankEntry1.setAmountFloat(" -  40,00");
        mBankEntry1.setDateFromString("30/03/2015");

        List<ParsedEntry> ynabTransactions = Arrays.asList(ynabEntry1, ynabEntry2);
        List<ParsedEntry> mbankTransactions = Arrays.asList(mBankEntry1);

        // # YNAB REDUNDANT Transactions
        List<ParsedEntry> ynabRedundant = new ArrayList<>();
        ynabRedundant.addAll(ynabTransactions);
        ynabRedundant.removeAll(mbankTransactions);

        Assertions.assertEquals(1, ynabRedundant.size());
    }

    @Test
    public void testYnabRedundant2() {
        ParsedEntry ynabEntry1 = new ParsedEntry();
        ynabEntry1.setAmountFloat(9.57);
        ynabEntry1.setDateFromString("12/03/2015");

        ParsedEntry ynabEntry2 = new ParsedEntry();
        ynabEntry2.setAmountFloat(-45.55);
        ynabEntry2.setDateFromString("30/03/2015");

        ParsedEntry mBankEntry1 = new ParsedEntry();
        mBankEntry1.setAmountFloat(" -  45,55");
        mBankEntry1.setDateFromString("30/03/2015");

        List<ParsedEntry> ynabTransactions = Arrays.asList(ynabEntry1, ynabEntry2);
        List<ParsedEntry> mbankTransactions = Arrays.asList(mBankEntry1);

        // # YNAB REDUNDANT Transactions
        List<ParsedEntry> ynabRedundant = new ArrayList<>();
        ynabRedundant.addAll(ynabTransactions);
        ynabRedundant.removeAll(mbankTransactions);

        Assertions.assertEquals(1, ynabRedundant.size());
    }

    @Test
    public void testMultipleTransactionMatchThroughTheDay() {
        // Only a single entry in YNAB
        // But few entries in mBank
        // Diff should detect this by applying counts

        ParsedEntry ynabEntry1 = new ParsedEntry();
        ynabEntry1.setAmountFloat(-123.99);
        ynabEntry1.setDateFromString("30/05/2015");

        ParsedEntry mBankEntry1 = new ParsedEntry();
        mBankEntry1.setAmountFloat(-123.99);
        mBankEntry1.setDateFromString("30/05/2015");

        ParsedEntry mBankEntry2 = new ParsedEntry();
        mBankEntry2.setAmountFloat(-123.99);
        mBankEntry2.setDateFromString("30/05/2015");

        ParsedEntry mBankEntry3 = new ParsedEntry();
        mBankEntry3.setAmountFloat(-123.99);
        mBankEntry3.setDateFromString("30/05/2015");

        List<ParsedEntry> ynabTransactions = Arrays.asList(ynabEntry1);
        List<ParsedEntry> mbankTransactions = Arrays.asList(mBankEntry1, mBankEntry2, mBankEntry3);


        Collection<ParsedEntry> missingTransactions = ynabDiff.findMissingTransactionsByCount(mbankTransactions, ynabTransactions);
        Assertions.assertEquals(3, missingTransactions.size());

        missingTransactions = ynabDiff.findMissingTransactionsByCount(ynabTransactions, mbankTransactions);
        Assertions.assertEquals(0, missingTransactions.size());
    }

    @Test
    public void testCountDiff() {
        ParsedEntry mBankEntry1 = new ParsedEntry();
        mBankEntry1.setAmountFloat(-123.99);
        mBankEntry1.setDateFromString("30/05/2015");

        ParsedEntry mBankEntry2 = new ParsedEntry();
        mBankEntry2.setAmountFloat(-123.99);
        mBankEntry2.setDateFromString("30/05/2015");

        ParsedEntry mBankEntry3 = new ParsedEntry();
        mBankEntry3.setAmountFloat(-79.54);
        mBankEntry3.setDateFromString("30/05/2015");

        Integer key1 = mBankEntry1.getAmount();
        Integer key2 = mBankEntry3.getAmount();

        List<ParsedEntry> mbankTransactions = Arrays.asList(mBankEntry1, mBankEntry2, mBankEntry3);
        final Map<Integer, Integer> priceToCount = ynabDiff.countPriceEntries(mbankTransactions);

        Assertions.assertEquals(2, priceToCount.size());
        Assertions.assertEquals(2, priceToCount.get(key1));
        Assertions.assertEquals(1, priceToCount.get(key2));
    }

    /////////////////////////////////////////////////////////////////////////////////////

    private Date asDate(String dateAsString) {
        try {
            return new SimpleDateFormat("dd/MM/yyyy").parse(dateAsString);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}
